Simulation Conditions:
- The Nematoda candidate sequences must be grouped in either two sets of two or two sets of one and a set of two
	at the beginning of the analysis.
- Sequences that produce no canary sequences, or four canary sequences are discounted from the analysis, as they do
	not test the efficacy of the method, only generate scenarios in which it is not applicable.
- For a 'Success':
	- A monophyletic Nematoda or Nematoidea, 
		must be resolved by the end of the analysis, within the Ecdysozoa
		and basal to the arthropoda as defined by the base tree of that given analysis.