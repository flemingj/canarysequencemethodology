
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			Sim.43
. Data set: 				#1
. Tree topology search : 		SPRs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	JC69
. Number of taxa: 			49
. Log-likelihood: 			-40008.38021
. Unconstrained likelihood: 		-10280.87792
. Parsimony: 				9321
. Tree size: 				7.06867
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.314
. Nucleotides frequencies:
  - f(A)= 0.25000
  - f(C)= 0.25000
  - f(G)= 0.25000
  - f(T)= 0.25000

. Run ID:				none
. Random seed:				1516487081
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h2m6s (126 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
