
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			Sim.05
. Data set: 				#1
. Tree topology search : 		SPRs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	JC69
. Number of taxa: 			49
. Log-likelihood: 			-40525.39002
. Unconstrained likelihood: 		-10495.78865
. Parsimony: 				9437
. Tree size: 				7.20740
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.333
. Nucleotides frequencies:
  - f(A)= 0.25000
  - f(C)= 0.25000
  - f(G)= 0.25000
  - f(T)= 0.25000

. Run ID:				none
. Random seed:				1516460344
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h1m7s (67 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
