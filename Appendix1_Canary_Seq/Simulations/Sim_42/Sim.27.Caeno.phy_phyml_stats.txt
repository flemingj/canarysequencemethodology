
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			Sim.27.Caeno.phy
. Data set: 				#1
. Tree topology search : 		SPRs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	JC69
. Number of taxa: 			46
. Log-likelihood: 			-38656.17934
. Unconstrained likelihood: 		-10160.46179
. Parsimony: 				8994
. Tree size: 				6.00992
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.289
. Nucleotides frequencies:
  - f(A)= 0.25000
  - f(C)= 0.25000
  - f(G)= 0.25000
  - f(T)= 0.25000

. Run ID:				none
. Random seed:				1516363199
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h1m2s (62 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
