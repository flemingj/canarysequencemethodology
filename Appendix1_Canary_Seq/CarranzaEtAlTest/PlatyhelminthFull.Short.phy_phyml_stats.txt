
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			PlatyhelminthFull.Short.phy
. Data set: 				#1
. Tree topology search : 		NNIs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			19
. Log-likelihood: 			-18127.84358
. Unconstrained likelihood: 		-15385.74066
. Parsimony: 				3616
. Tree size: 				4.52401
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.393
. Transition/transversion ratio: 	3.169
. Nucleotides frequencies:
  - f(A)= 0.24797
  - f(C)= 0.22214
  - f(G)= 0.27648
  - f(T)= 0.25342

. Run ID:				none
. Random seed:				1524579243
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m9s (9 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
